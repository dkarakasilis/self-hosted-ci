#!/bin/bash

set -e

# Useful to understand this bash foo: https://stackoverflow.com/a/43192740
# Sample input to the functions in this script:
#{
#  "services": [
#    {
#      "bindIP": "192.168.6.1",
#      "bindPort": "443",
#      "proxyAddress": "traefik.kube-system.svc",
#      "proxyPort": "443"
#    },
#    {
#      "bindIP": "192.168.6.1",
#      "bindPort": "80",
#      "proxyAddress": "traefik.kube-system.svc",
#      "proxyPort": "80"
#    }
#  ],
#  "jumpbox": {
#    "url": "1.2.3.4",
#    "user": "root",
#    "sshKeyFile": ".ssh/mykey"
#  }
#}

generateNginxConfig() {
  servers=$(cat "$1" | jq -r '.services[] | [.bindPort, .proxyAddress, .proxyPort] | @tsv' | while IFS=$'\t' read -r bindPort proxyAddress proxyPort; do
    echo "
      server {
          listen $bindPort;
          proxy_pass $proxyAddress:$proxyPort;
      }
  "
  done
  )

  cat << EOF
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
        worker_connections 768;
}

stream {
$servers
}
EOF

}

SSHTunnelingCommand() {
  tunnels=$(cat "$1" | jq -r '.services[] | [.bindIP, .bindPort, .proxyAddress, .proxyPort] | @tsv' | while IFS=$'\t' read -r bindIP bindPort proxyAddress proxyPort; do
    printf " -R $bindIP:$bindPort:127.0.0.1:$bindPort "
  done
  )

  jumpboxSSHKey="$(cat "$1" | jq -r '.jumpbox.sshKeyFile')"
  jumpboxURL="$(cat "$1" | jq -r '.jumpbox.url')"
  jumpboxUser="$(cat "$1" | jq -r '.jumpbox.user')"

  echo "ssh -o \"StrictHostKeyChecking no\" -i ${jumpboxSSHKey} -f -N ${tunnels} ${jumpboxUser}@${jumpboxURL}"
}

# Run nginx (goes to the background too)
startNginx() {
  nginx
}

main() {
  generateNginxConfig $@ > /etc/nginx/nginx.conf
  eval $(SSHTunnelingCommand $@)
  startNginx

  # Keep the container alive
  exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
}

# Allow sourcing this script without running "main" for debugging
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi
